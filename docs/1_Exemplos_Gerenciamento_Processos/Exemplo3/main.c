#include <windows.h>
#include <stdio.h>

int main(void)
{  
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
    int i;
// Initialize structures.
   ZeroMemory( &si, sizeof(STARTUPINFO) );
   ZeroMemory( &pi, sizeof(PROCESS_INFORMATION) );

   si.cb          = sizeof( STARTUPINFO );
   si.dwFlags     = STARTF_USESHOWWINDOW;
   si.wShowWindow = SW_SHOWNORMAL;
   for(i=0;i<5;i++) printf("vai criar processo......\n");

// Start the child process.
if( !CreateProcess( NULL, // No module name (use command line)
	    "C:/processo2.exe", 
		NULL, // Process handle not inheritable
		NULL, // Thread handle not inheritable
		FALSE, // Set handle inheritance to FALSE
		//0, // No creation flags
		CREATE_NEW_CONSOLE, //DETACHED_PROCESS, 
		NULL, // Use parent's environment block
		NULL, // Use parent's starting directory
		&si, // Pointer to STARTUPINFO structure
		&pi ) // Pointer to PROCESS_INFORMATION structure
)  printf( "CreateProcess failed\n"); 
else printf( "CreateProcess executou com sucesso\n"); 
// Wait until child process exits.
WaitForSingleObject( pi.hProcess, INFINITE );
// Close process and thread handles.
CloseHandle( pi.hProcess );
CloseHandle( pi.hThread );

   for(i=0;i<5;i++) printf("Vai sair - \n");

printf("Programa principal - digite qualquer tecla - \n");
_getch();

return(0);
	
}

