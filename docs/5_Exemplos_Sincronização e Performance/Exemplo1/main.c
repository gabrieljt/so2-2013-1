#include <windows.h>
#include <stdio.h>
#include <assert.h>

CRITICAL_SECTION regiao_print;

HANDLE Thread_Handles[100];

///////////// Thread Main ///////////////////////
void ThreadMain(int nth)
    { 
	int i, x;
	//while (1) x = x;
    for(i=1;i<1000;i++)
        { 
		EnterCriticalSection (&regiao_print);
        printf("Printing Thread %d\n", nth); 
        LeaveCriticalSection (&regiao_print);

        Sleep(1); 
        }
    }

///////////////////// Create A Child//////////////
void CreateChild(int n)
    {     
    DWORD dwId;
    Thread_Handles[n]=CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)ThreadMain
                            ,(LPVOID)n,0,&dwId); 
    }

//////////////////// Main ///////////////////////
int main(void)
    {
	int i;
	InitializeCriticalSection (&regiao_print);

    for (i=0;i<2;i++) CreateChild(i);

	WaitForMultipleObjects(2, Thread_Handles,
    TRUE, INFINITE);
   
    return 0;
    }
