#include <windows.h>
#include <stdio.h>
#include <assert.h>

HANDLE hMutex; 

HANDLE Thread_Handles[100];

///////////// Thread Main ///////////////////////
void ThreadMain(int nth)
    { 
	int i;
    for(i=1;i<1000;i++)
        { 
		WaitForSingleObject( hMutex, // handle to mutex
						     INFINITE);// five-second time-out interval

        printf("Printing Thread %d\n", nth);
        ReleaseMutex(hMutex);

        Sleep(10); 
        }
    }

///////////////////// Create A Child//////////////
void CreateChild(int n)
    {    
    DWORD dwId;
    Thread_Handles[n]=CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)ThreadMain
                            ,(LPVOID)n,
							CREATE_SUSPENDED,&dwId); 
     
	 if (n % 2 == 0)  SetThreadAffinityMask(Thread_Handles[n], 0x01);
	 else  SetThreadAffinityMask(Thread_Handles[n], 0x02);
	 
	// SetThreadPriority(Thread_Handles[n],THREAD_PRIORITY_TIME_CRITICAL);
     ResumeThread(Thread_Handles[n]);
    }

//////////////////// Main ///////////////////////
int main(void)
    {
	int i;
	// Create a mutex with no initial owner.
    hMutex = CreateMutex(NULL,                    // no security attributes
                         FALSE,                   // initially not owned
                         "MutexToProtectCount");  // name of mutex

    if (hMutex == NULL) 
        printf("Erro na criacao do Mutex\n");

    for (i=0;i<2;i++) CreateChild(i);
	WaitForMultipleObjects(2, Thread_Handles,
    TRUE, INFINITE);
  
    return 0;
    }
