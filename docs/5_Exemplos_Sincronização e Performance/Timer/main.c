#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <stdlib.h>
#include <malloc.h>
#include <io.h>
#include <process.h>

int main(void)
{  
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
  
    union {	/* Structure required for file time arithmetic. */
		   LONGLONG li;
		   FILETIME ft;
	       } CreateTime, ExitTime, ElapsedTime;

	FILETIME KernelTime, UserTime;
	SYSTEMTIME ElTiSys, KeTiSys, UsTiSys; 

// Initialize structures.
   ZeroMemory( &si, sizeof(STARTUPINFO) );
   ZeroMemory( &pi, sizeof(PROCESS_INFORMATION) );

   si.cb          = sizeof( STARTUPINFO );
   si.dwFlags     = STARTF_USESHOWWINDOW;
   si.wShowWindow = SW_SHOWNORMAL;

// Start the child process.
   if( !CreateProcess( NULL, // No module name (use command line)
	    "C:/processo1.exe", 
		NULL, // Process handle not inheritable
		NULL, // Thread handle not inheritable
		FALSE, // Set handle inheritance to FALSE
		//0, // No creation flags
		CREATE_NEW_CONSOLE,
		NULL, // Use parent's environment block
		NULL, // Use parent's starting directory
		&si, // Pointer to STARTUPINFO structure
		&pi ) // Pointer to PROCESS_INFORMATION structure
        )  printf( "CreateProcess failed\n"); 
/////////////////////////////////////////////////////
// Wait until child process exits.
WaitForSingleObject( pi.hProcess, INFINITE );
//printf("Esperando no WaitForSingleObject - \n");
if (!GetProcessTimes (pi.hProcess, &CreateTime.ft,
			          &ExitTime.ft, &KernelTime, 
					  &UserTime))
  printf("Erro - n�o pode pegar o tempo do processo - \n");


        ElapsedTime.li = ExitTime.li - CreateTime.li;
////
        FileTimeToSystemTime (&ElapsedTime.ft, &ElTiSys);
		FileTimeToSystemTime (&KernelTime, &KeTiSys);
		FileTimeToSystemTime (&UserTime, &UsTiSys);
        printf("Tempo Total:  %02d:%02d:%02d:%03d\n",
			            ElTiSys.wHour, 
			            ElTiSys.wMinute, 
			            ElTiSys.wSecond,
			            ElTiSys.wMilliseconds);

		printf("User Time:    %02d:%02d:%02d:%03d\n",
			            UsTiSys.wHour, 
						UsTiSys.wMinute, 
						UsTiSys.wSecond,
			            UsTiSys.wMilliseconds);
		printf("System Time:  %02d:%02d:%02d:%03d\n",
		            	KeTiSys.wHour, 
						KeTiSys.wMinute, 
						KeTiSys.wSecond,
			            KeTiSys.wMilliseconds);
////

// Close process and thread handles.
CloseHandle( pi.hProcess );
CloseHandle( pi.hThread );

printf("Programa principal - digite qualquer tecla - \n");
_getch();

return(0);
	
}

