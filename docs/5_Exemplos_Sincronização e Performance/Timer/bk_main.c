#include <windows.h>
#include <stdio.h>
///
#include <tchar.h>
#include <stdlib.h>
#include <malloc.h>
#include <io.h>
#include <process.h>

///
int main(void)
{  
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
    int i;
	int c;
	HANDLE h;
    
	FILETIME CreateTime, ExitTime, ElapsedTime, KernelTime, UserTime;
	SYSTEMTIME ElTiSys, KeTiSys, UsTiSys, StartTimeSys, ExitTimeSys;

// Initialize structures.
   ZeroMemory( &si, sizeof(STARTUPINFO) );
   ZeroMemory( &pi, sizeof(PROCESS_INFORMATION) );

   si.cb          = sizeof( STARTUPINFO );
   si.dwFlags     = STARTF_USESHOWWINDOW;
   si.wShowWindow = SW_SHOWNORMAL;
   for(i=1;i<10;i++) printf("vai criar processo......\n");


// Start the child process.
if( !CreateProcess( NULL, // No module name (use command line)
	//	"C:/WINDOWS/system32/notepad.exe", // Command line
	    "C:/processo1.exe", 
		NULL, // Process handle not inheritable
		NULL, // Thread handle not inheritable
		FALSE, // Set handle inheritance to FALSE
		0, // No creation flags
		NULL, // Use parent's environment block
		NULL, // Use parent's starting directory
		&si, // Pointer to STARTUPINFO structure
		&pi ) // Pointer to PROCESS_INFORMATION structure
)  printf( "CreateProcess failed\n"); 
else printf( "CreateProcess executou com sucesso\n");
/////////////////////////////////////////////////////

/////////////////////////////////////////////////////
// Wait until child process exits.
WaitForSingleObject( pi.hProcess, INFINITE );
if (!GetProcessTimes (pi.hProcess, &CreateTime,
			          &ExitTime, &KernelTime, 
					  &UserTime))
  printf("Erro - n�o pode pegar o tempo do processo - \n");


ElapsedTime = ExitTime - CreateTime;
////
        FileTimeToSystemTime (&ElapsedTime, &ElTiSys);
		FileTimeToSystemTime (&KernelTime, &KeTiSys);
		FileTimeToSystemTime (&UserTime, &UsTiSys);
		_tprintf (_T ("Real Time: %02d:%02d:%02d:%03d\n"),
			ElTiSys.wHour, ElTiSys.wMinute, ElTiSys.wSecond,
			ElTiSys.wMilliseconds);
		_tprintf (_T ("User Time: %02d:%02d:%02d:%03d\n"),
			UsTiSys.wHour, UsTiSys.wMinute, UsTiSys.wSecond,
			UsTiSys.wMilliseconds);
		_tprintf (_T ("Sys Time:  %02d:%02d:%02d:%03d\n"),
			KeTiSys.wHour, KeTiSys.wMinute, KeTiSys.wSecond,
			KeTiSys.wMilliseconds);

////

// Close process and thread handles.
CloseHandle( pi.hProcess );
CloseHandle( pi.hThread );
	

   for(i=1;i<10;i++) printf("Vai sair - \n");
//Sleep (10000);
printf("Programa principal - digite qualquer tecla - \n");
_getch();

return(0);
	
}

