#include <windows.h>
#include <stdio.h>
#include <assert.h>

CRITICAL_SECTION regiao_print;

HANDLE Thread_Handles[100];
//volatile LONG TotalCountOfPrints=0;
//int i;

void delay_cpu(void) 
{
	int i=0, j=0;
	/* Do some wasteful computations that will not be optimized to nothing */
	while (i < 20) {
		          j = (int)(i*i + (float)(2*j)/(float)(i+1));
		          i++;
	               }
}


///////////// Thread Main ///////////////////////
void ThreadMain(int nth)
    { 
	int i, x=1;
	printf("THREAD %d  EXECUTANDO\n\n", nth);
	while (1) x = x;
    for(i=1;i<1000;i++)
        { 
			delay_cpu;
		EnterCriticalSection (&regiao_print);
        printf("Printing Thread %d\n", nth);  
		x++;
        LeaveCriticalSection (&regiao_print);

       // Sleep(rand()%10);
		Sleep(10);
        }
    }

///////////////////// Create A Child//////////////
void CreateChild(int n)
    {    
	int contador=1;
    DWORD dwId;
    Thread_Handles[n]=CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)ThreadMain
                            ,(LPVOID)n,
							CREATE_SUSPENDED,&dwId); 
     
	 if (n % 2 == 0)  SetThreadAffinityMask(Thread_Handles[n], 0x01);
	 else   SetThreadAffinityMask(Thread_Handles[n], 0x01);
	        
     ResumeThread(Thread_Handles[n]);
    }

//////////////////// Main ///////////////////////
int main(void)
    {
	int i;
	InitializeCriticalSectionAndSpinCount(&regiao_print, 10); 
	//InitializeCriticalSection (&regiao_print);

    for (i=0;i<40;i++) CreateChild(i);

	WaitForMultipleObjects(40, Thread_Handles,
    TRUE, INFINITE);
   
    return 0;
    }
