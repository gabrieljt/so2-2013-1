#include <windows.h> 
#include <stdio.h>
#define BUFSIZE 1024
#define PIPE_TIMEOUT 5000

int main()
{
	HANDLE hFile;
    BOOL flg;
    DWORD dwWrite; 
    char buffer_write[BUFSIZE]="Dado do Cliente para o Servidor"; 
	while(!WaitNamedPipe("\\\\.\\pipe\\mynamedpipe", INFINITE)) Sleep(100);
    hFile = CreateFile("\\\\.\\pipe\\mynamedpipe", GENERIC_WRITE,
                                     0, NULL, OPEN_EXISTING,
                                     0, NULL);

          if(hFile == INVALID_HANDLE_VALUE)
          { 
              printf("CreateFile failed for Named Pipe client\n" );
          }
          else
          {
              flg = WriteFile(hFile, buffer_write, strlen(buffer_write), &dwWrite, NULL);
              if (FALSE == flg)
              {
                 printf("WriteFile falhou .....\n");
              }
              else
              {
                 printf("WriteFile executou com sucesso .....\n");
              }

              CloseHandle(hFile);
          }
  _getch();

}

