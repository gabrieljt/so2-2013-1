// Exemplo de Anonymous Pipes Win32

#include <windows.h>
#include <assert.h>
#include <stdio.h>
#include <conio.h>

#define	PIPE_BUFFERLENGTH	40

typedef struct {
	HANDLE hWrThread;
	HANDLE hRdPipe;
}ThreadData;

void ThreadFunc(ThreadData *lpVoid) 
{
	HANDLE hReadPipe = lpVoid->hRdPipe;
	HANDLE hWrThread = lpVoid->hWrThread;

	char ReadBuff[PIPE_BUFFERLENGTH];
	BOOL bRet = FALSE;
	DWORD dwRead;
        
	printf("thread vai ler do pipe\n");
	bRet = ReadFile(hReadPipe,&ReadBuff,sizeof(ReadBuff),&dwRead,NULL);
	printf("Thread Leu:  %s\n",ReadBuff);
		
	CloseHandle(hReadPipe);
	ExitThread(0);
}


int main() 
{
	HANDLE	hReadPipe = NULL;		// Handle to a read end of a pipe.
	HANDLE	hWritePipe = NULL;		// Handle to a write end of a pipe.
	HANDLE  hRdThread = NULL;		// Handle to a reader thread.
	BOOL    bReturn = FALSE;		
	DWORD   dwThreadID;				// Thread ID.
	HANDLE  hWrThread = NULL;		// Handle to a writer thread.
	HANDLE  hWrDupl = NULL;			// Duplicate handle to a writer thread.

	ThreadData thdData;				// Data to be sent to Reader thread.
    char Buff[PIPE_BUFFERLENGTH] = "Primeira msg escrita no anonymous Pipe";
	DWORD dwWrite;		// Actual data written to the pipe.
	BOOL bRet = FALSE;

	printf("main vai criar o anonymous pipe\n");
	bReturn = CreatePipe(&hReadPipe,&hWritePipe,NULL,PIPE_BUFFERLENGTH);
	
	if (bReturn != FALSE) {
		thdData.hRdPipe = hReadPipe;
		hWrThread = GetCurrentThread();

		printf("Main vai duplicar o manipulador do thread primario\n");
		bReturn = DuplicateHandle(GetCurrentProcess(),hWrThread,GetCurrentProcess(),&hWrDupl,NULL,FALSE,DUPLICATE_SAME_ACCESS);

		thdData.hWrThread = hWrDupl;

		if (bReturn != FALSE) {
			// Creates a reader thread, which act as a pipe client and
			// passing the ThreadData structure to the reader thread.
            printf("main vai criar o thread Cliente\n");
			hRdThread = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)ThreadFunc,(LPVOID)&thdData,0,&dwThreadID);
		
			// Writing data on the pipe.	
                printf("main vai escrever no  pipe\n");
				bRet = WriteFile(hWritePipe,Buff,sizeof(Buff),&dwWrite,NULL);
			                  }
	         	}

		CloseHandle(hWritePipe);
		
		// Wait for reader thread to finish its work and exit itself.
        printf("Main vai esperar o thread terminar\n");
		WaitForSingleObject(hRdThread,INFINITE);
	
	_getch();

	return 0;
}
	