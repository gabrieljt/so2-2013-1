#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <windows.h>

#define	PIPE_TIMEOUT	400000
#define BUFSIZE 50
#define BUFSIZEp 19
 
VOID InstanceThread(LPVOID); 

DWORD main(VOID) 
{ 
   BOOL fConnected; 
   DWORD dwThreadId; 
   HANDLE hPipe, hThread; 
   LPTSTR lpszPipename = "\\\\.\\pipe\\mynamedpipe";
   
 
      hPipe = CreateNamedPipe( 
          lpszPipename,             // pipe name 
          PIPE_ACCESS_DUPLEX,       // read/write access 
          PIPE_TYPE_MESSAGE |       // message type pipe 
          PIPE_READMODE_MESSAGE |   // message-read mode 
          PIPE_WAIT,                // blocking mode 
          PIPE_UNLIMITED_INSTANCES, // max. instances  
          BUFSIZE,                  // output buffer size 
          BUFSIZE,                  // input buffer size 
          PIPE_TIMEOUT,             // client time-out 
          NULL);                    // no security attribute 
 
 printf("Executou CreateNamedPipe e espera agora conexao\n");
      
 fConnected = ConnectNamedPipe(hPipe, NULL); 
      if (fConnected) 
      { 
printf("Vai Executar CreateThread\n");
      // Create a thread for this client. 
         hThread = CreateThread( 
            NULL,              // no security attribute 
            0,                 // default stack size 
            (LPTHREAD_START_ROUTINE) InstanceThread, 
            (LPVOID) hPipe,    // thread parameter 
            0,                 // not suspended 
            &dwThreadId);      // returns thread ID 
      } 
      else 
	  {printf("client could not connect\n");
        // The client could not connect, so close the pipe. 
         CloseHandle(hPipe);
	  }
   printf("Esperando  uma tecla para sair - ESCRITA\n");
 _getch();

   return 1; 
} 
 
VOID InstanceThread(LPVOID lpvParam) 
{ 
   CHAR chRequest[BUFSIZE]; 
   CHAR chReply[BUFSIZE]="..Resposta do Thread servidor.."; 
   DWORD cbBytesRead, cbReplyBytes, cbWritten; 
   BOOL fSuccess; 
   HANDLE hPipe; 
 
// The thread's parameter is a handle to a pipe instance. 
 
   hPipe = (HANDLE) lpvParam; 
 
   // Read client requests from the pipe.
printf("Thread -  Read client requests from the pipe\n");
      fSuccess = ReadFile( 
         hPipe,        // handle to pipe 
         chRequest,    // buffer to receive data 
         BUFSIZE,      // size of buffer 
         &cbBytesRead, // number of bytes read 
         NULL);        // not overlapped I/O 

printf("Thread Servidor Leu:  %s\n",chRequest);

// Flush the pipe to allow the client to read the pipe's contents 
// before disconnecting. Then disconnect the pipe, and close the 
// handle to this pipe instance. 
 
   FlushFileBuffers(hPipe); 
   DisconnectNamedPipe(hPipe); 
   CloseHandle(hPipe); 
}

