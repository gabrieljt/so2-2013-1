#include <windows.h>
#include <stdio.h>

int main(int argc,char **argv)
{
	HANDLE		handle;
	LPCTSTR		errMsg;

	handle = CreateMailslot("\\\\.\\mailslot\\my_mailslot",
							0,
							MAILSLOT_WAIT_FOREVER,
							NULL);
	if (handle == INVALID_HANDLE_VALUE) 
	{
		TCHAR	strError[256];

		errMsg = "CreateMailslot failed: %s";
print_err:
		// Get the error message associated with this error # from Windows
		FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), &strError[0], 256, 0);

		// Display it
		printf(errMsg, &strError[0]);

		// Close any mailslot we opened
		if (handle != INVALID_HANDLE_VALUE) CloseHandle(handle);

		return(-1);
	}

	for (;;)
	{
		DWORD	msgSize;
		BOOL	err;

		/* Get the size of the next record */
		err = GetMailslotInfo(handle, 0, &msgSize, 0, 0);

		/* Check for an error */
		if (!err)
		{
			errMsg = "GetMailslotInfo failed: %s";
			goto print_err;
		}
			
		/* Check if there was a record. If so, the size is not -1 */
		if (msgSize != (DWORD)MAILSLOT_NO_MESSAGE)
		{
			void *	buffer;

			/* Allocate memory to read in the record */
			buffer = GlobalAlloc(GMEM_FIXED, msgSize);
			if (!buffer) printf("An error getting a memory block!");
			else
			{
				DWORD	numRead;

				/* Read the record */
printf("Vai para a leitura, esperando dados ...\n");
				err = ReadFile(handle, buffer, msgSize, &numRead, 0);

				/* See if an error */
				if (!err) printf("ReadFile error: %d", GetLastError());

				/* Make sure all the bytes were read */
				else if (msgSize != numRead) printf("ReadFile did not read the correct number of bytes!");

				else
				{
					/* "buffer" now contains the contents of the record. We expect
					 * clientslot.c to send us a nul-terminated string
					 */
					printf(buffer);
				}

				/* Free the buffer */
				GlobalFree(buffer);
			}
		}

		/* Before we get the next record, just put a Sleep() in here so not to use too much CPU time */
		Sleep(1000);
	}

	return(0);
}
