// Segundo Exemplo de Anonymous Pipes Win32

#include <windows.h>
#include <assert.h>
#include <stdio.h>
#include <conio.h>

#define	PIPE_BUFFERLENGTH	19

typedef struct  {
	HANDLE hWrThread;
	HANDLE hRdPipe;
    HANDLE hWrPipe;
} ThreadData;

void ThreadFunc(ThreadData *lpVoid) 
{
	HANDLE hReadPipe = lpVoid->hRdPipe;
	HANDLE hWritePipe = lpVoid->hWrPipe;
	HANDLE hWrThread = lpVoid->hWrThread;
    
		char ReadBuff[40];
		char Buf [40] = "msg do Thread para o main";
		BOOL bRet = FALSE;
		DWORD dwRead;
		DWORD dwWrite;

        printf("thread vai ler do pipe\n");
		bRet = ReadFile(hReadPipe,&ReadBuff,sizeof(ReadBuff),&dwRead,NULL);

		printf("Thread Leu:  %s\n",ReadBuff);
		printf("Thread - Bytes Lido :%d\n",dwRead);

        bRet = WriteFile(hWritePipe,Buf,sizeof(Buf),&dwWrite,NULL);
		printf("Thread - Bytes Escritos :%d\n",dwWrite);
		
	CloseHandle(hReadPipe);
	ExitThread(0);
}


int main() 
{
	HANDLE	hReadPipe = NULL;		// Handle to a read end of a pipe.
	HANDLE	hWritePipe = NULL;		// Handle to a write end of a pipe.
	HANDLE  hRdThread = NULL;		// Handle to a reader thread.
	BOOL    bReturn = FALSE;		
	DWORD   dwThreadID;				// Thread ID.
	HANDLE  hWrThread = NULL;		// Handle to a writer thread.
	HANDLE  hWrDupl = NULL;			// Duplicate handle to a writer
									// thread.
	char ReadBuff[40];
	BOOL bRet = FALSE;
	ThreadData thdData;				// Data to be sent to Reader thread.
    DWORD dwRead;
	DWORD dwWrite;		// Actual data written to the pipe.
	char Buff[40] = "Primeira msg escrita no anonymous Pipe";
	
	bReturn = CreatePipe(&hReadPipe,&hWritePipe,NULL,PIPE_BUFFERLENGTH);

	if (bReturn != FALSE) {
		thdData.hRdPipe = hReadPipe;
		thdData.hWrPipe = hWritePipe;
			
		hWrThread = GetCurrentThread();

		bReturn = DuplicateHandle(GetCurrentProcess(),hWrThread,GetCurrentProcess(),&hWrDupl,NULL,FALSE,DUPLICATE_SAME_ACCESS);

		thdData.hWrThread = hWrDupl;

		if (bReturn != FALSE) {
			hRdThread = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)ThreadFunc,(LPVOID)&thdData,0,&dwThreadID);

				bRet = FALSE;
				
				bRet = WriteFile(hWritePipe,Buff,sizeof(Buff),&dwWrite,NULL);
				printf("main - Bytes Escritos :%d\n",dwWrite);
			                  }
	         	}

	bRet = ReadFile(hReadPipe,&ReadBuff,sizeof(ReadBuff),&dwRead,NULL);
	printf("main has Read :%s\n",ReadBuff);
	printf("Main - Bytes Lido :%d\n",dwRead);


		CloseHandle(hWritePipe);
		
		WaitForSingleObject(hRdThread,INFINITE);
	
	_getch();

	return 0;
}
	