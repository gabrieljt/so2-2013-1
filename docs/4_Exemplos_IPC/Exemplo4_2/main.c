#include <windows.h>
#include <stdio.h>

int main(int argc,char **argv)
{
	HANDLE		handle;
	LPCTSTR		errMsg;
	BOOL	    err;
	DWORD		numWritten;
	static TCHAR MyMessage[] = {"Mensagem enviada pelo Cliente ....\r\n"};

	handle = CreateFile("\\\\.\\mailslot\\my_mailslot",
						GENERIC_WRITE, 
						FILE_SHARE_READ,
						0, 
						OPEN_EXISTING, 
						FILE_ATTRIBUTE_NORMAL, 
						0); 
	if (handle == INVALID_HANDLE_VALUE) 
	{
		TCHAR	strError[256];

		errMsg = "CreateFile failed: %s";
print_err:
		// Get the error message associated with this error # from Windows
		FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), &strError[0], 256, 0);

		// Display it
		printf(errMsg, &strError[0]);

		// Close any mailslot we opened
		if (handle != INVALID_HANDLE_VALUE) CloseHandle(handle);
printf("Esperando  uma tecla para sair - ESCRITA\n");
 _getch();
		return(-1);
	}

	err = WriteFile(handle, MyMessage, sizeof(MyMessage), &numWritten, 0);

	/* See if an error */
	if (!err)
	{
		errMsg = "WriteFile failed: %s";
		goto print_err;
	}

	/* Make sure all the bytes were written */
	if (sizeof(MyMessage) != numWritten) printf("WriteFile did not read the correct number of bytes!\n");

	printf("Esperando  uma tecla para sair - ESCRITA\n");
 _getch();

	return(0);
}
