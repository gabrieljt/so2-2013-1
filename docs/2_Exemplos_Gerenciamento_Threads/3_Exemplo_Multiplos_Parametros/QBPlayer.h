#include <cstdlib>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "DSBeeper.h"

/**********************
 * QBPlayer - An implementation of the QBASIC `Play' command
 *
 * Requires a (supplied) Beep class. On non-64 bit machines,
 * use Win32Beep, otherwise use DSBeeper which goes through
 * DirectSound.
 *
 * An example of using this is as follows:
 *   QBPlayer<DSBeeper> player;
 *   player.Play("CDEFGAB");
 *
 * The format for the play string is given below. Unlike in
 * QBASIC, errors are silently ignored, so incorrectly formatted
 * strings will still play, though probably not in a way you
 * expect.
 */

/*
This is taken from the QBASIC help documents, along with some extra annotations of mine.

     commandstring$    A string expression that contains one or more of
                        the following PLAY commands:

      Octave and tone commands:
        Ooctave    Sets the current octave (0 - 6).
        < or >     Moves up or down one octave.
        A - G      Plays the specified note in the current octave.
        Nnote      Plays a specified note (0 - 84) in the seven-octave
                   range (0 is a rest).

      Duration and tempo commands:
        Llength    Sets the length of each note (1 - 64). L1 is whole note,
                   L2 is a half note, etc.
        ML         Sets music legato.
        MN         Sets music normal.
        MS         Sets music staccato.
        Ppause     Specifies a pause (1 - 64). P1 is a whole-note pause,
                   P2 is a half-note pause, etc.
        Ttempo     Sets the tempo in quarter notes per minute (32 - 255).

      Mode commands:
        MF          Plays music in foreground.
        MB          Plays music in background.

      Suffix commands: (can be preceded by whitespace)
        # or +      Turns preceding note into a sharp. (cannot follow N#, can't have more than one, can't follow -, can't follow 'E' and 'B')
        -           Turns preceding note into a flat.  (cannot follow N#, can't have more than one, can't follow -, can't follow 'F' and 'C')
        .           Plays the preceding note 3/2 as long as specified. (affects A-G, P#, N#, MUST come after sharp or flag modifiers, can have more than one)

My notes:
  Whitespace is ignored.
  Octaves go CDEFGAB
*/
/*
The following is likely wrong, since I just wrote it up.

PLAY <command-string>

<whitespace> ::= nothing | { ' ' | '\t' }

<command-string> ::= <command-with-space> { <command-with-space> }
<command-with-space> ::= [ <whitespace> ] <command>
<command> ::= <single-char-command> | <AG-command> | <note-command> | <pause-command> | <number-arg-command> | <music-command>

<single-char-command> ::= '<' | '>'

<AG-command> ::= <AG-char> [ sharp-flat> ] { <longdot> }
<AG-char> ::= 'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G'
<sharp-flat> ::= { <whitespace> } '#' | '+' | '-'
<longdot> ::= { <whitespace> } '.'

<note-command> ::= 'N' <number> { <longdot> }

<pause-command> ::= 'P' <number> { <longdot> }

<number-arg-command> ::= <number-arg-command-char> <digit> { <digit> }
<number-arg-command-char> ::= 'O' | 'T' | 'L'
<number> ::= <digit> { <digit> }
<digit> ::=  { <whitespace> } '0' | ... | '9'

<music-command> ::= 'M' <music-char>
<music-char> ::= 'N' | 'L' | 'S' | 'F' | 'B'

*/
/*
Reverse engineering samples:

repeated "C" -> 1050 Hz, 0.517979 sec note interval, 0.452603 sec of actual frequency played (rest is pause)
repeated "L2 C" -> 1.034739 sec note interval, 0.905578 of actual playing
repeated "C." -> 0.773515 interval, 0.677732 playing
The above two indicate that "normal" mode is 87.5% (7/8) playing, and the rest pause.
It also indicates that the '.' is just a literal 3/2 stretching

repeated "MS C" -> 0.516644 interval, 0.386032 played, so staccato is 6/8 playtime
repeated "ML C" -> nonstop playing, so legato is 8/8 playtime
*/

/*

Todo: convert the parser to table driven code.

struct command_entry{
	bool (*CommandParser)(music_state *S, const char *cmdstr);
};

const command_entry[] = {
	{}
};
*/


template <class BeeperType>
class QBPlayer{
	BeeperType beeper;
	int octave; // 0-6, default 4
	int note_length; // 1-64
	int style; // 1 = normal, 2 = legato, 0 = staccato
	int tempo; // 32-255
	// no effect: (should we spin until it's done?)
	int foreground; // 0 = false (background), 1 = true (foreground)


	// in millisec
	int GetDuration(int length){
		int L = note_length;
		if(length != 0 && 1 <= length && length <= 64){
			L = length;
		}
		// for a given L, the # of quarter notes = 4/L
		// for a given T, the # of quarter notes/sec = T/60
		// # seconds = 60/T * 4/L = 240/(T*L)
		// # millisec = 240,000/T*L
		return 240000/(tempo * L);
	}
	int GetPlayDuration(int duration){
		return duration*(6+style)/8;
	}
	static void skip_whitespace(const char **p){ // assumes p is not NULL
		while(NULL != **p && isspace(**p)){ (*p)++; }
	}
	static void advance_skip_whitespace(const char **p){
		(*p)++;
		skip_whitespace(p);
	}
public:
	QBPlayer(){
		octave = 4;
		note_length = 4; // quarter note
		style = 1;
		tempo = 118; // this many quarter notes played in a row should take up one minute of time
		foreground = 1;
	}
	~QBPlayer(){
	}
		
	// Not up to spec; doesn't skip whitespace
	void Play(const char *cmdstr){
		// stored as C, C#, D, D#, E, F, F#, G, G#, A, A#, B, 12 per line
		// Cannot contain entries less than 37, since that is the minimum that Beep() accepts
		static const buffer_int_t note_freq[] = {
		//	  16,   17,   18,   19,   21,   22,   23,   25,   26,   28,   29,   31, // 0
		//	  33,   35,   37,   39,   41,   44,   46,   49,   52,   55,   58,   62, // 1
			  65,   69,   73,   78,   82,   87,   93,   98,  104,  110,  117,  123, // 2
			 131,  139,  147,  156,  165,  175,  185,  196,  208,  220,  233,  247, // octave 3
			 262,  277,  294,  311,  330,  349,  370,  392,  415,  440,  466,  494, // octave 4
			 523,  554,  587,  622,  659,  698,  740,  784,  831,  880,  932,  988, // 5
			1047, 1109, 1175, 1245, 1319, 1397, 1480, 1568, 1661, 1760, 1865, 1976, // 6 (this is octave 4 in visual basic, the default)
			2093, 2217, 2349, 2489, 2637, 2794, 2960, 3136, 3322, 3520, 3729, 3951, // 7
			4186, 4435, 4699, 4978, 5274, 5588, 5920, 6272, 6645, 7040, 7459, 7902  // 8
		};
		static const int note_to_offset[] = {
			 9, // A
			11, // B
			 0, // C
			 2, // D
			 4, // E
			 5, // F
			 7  // G
		};
		// this divided by 8 is proportion of total note interval to play
		//static const int style_number[] = {6, 7, 8};

		int i = 0;
		int a, b, c, d;
		const char *ch = cmdstr;
		while(NULL != *ch){
			switch(*ch){
			case 'o':
			case 'O': // octave
				advance_skip_whitespace(&ch);
				a = 0;
				while(NULL != *ch){ // potential overflow
					b = *ch - '0';
					if(0 <= b && b <= 9){
						a = 10*a + b;
						advance_skip_whitespace(&ch);
					}else{ break; }
				}
				if(a < 0){ a = 0; }
				if(a > 6){ a = 6; }
				octave = a;
				break;
			case '<': // down an octave
				a = octave-1;
				if(a < 0){ a = 0; }
				octave = a;
				advance_skip_whitespace(&ch);
				break;
			case '>': // up an octave
				a = octave+1;
				if(a > 6){ a = 6; }
				octave = a;
				advance_skip_whitespace(&ch);
				break;
			case 'A': // notes; can take a suffix
			case 'B':
			case 'C':
			case 'D':
			case 'E':
			case 'F':
			case 'G':
			case 'a': // notes; can take a suffix
			case 'b':
			case 'c':
			case 'd':
			case 'e':
			case 'f':
			case 'g':
				if(*ch >= 'a'){
					a = 'a';
				}else{
					a = 'A';
				}
				a = note_to_offset[(*ch - a)]; // column of the freq table
				d = GetDuration(0);
				advance_skip_whitespace(&ch);

				// suffix detection: one sharp/flat, any number of '.'
				if(NULL != *ch){
					if(('#' == *ch || '+' == *ch) && (4 != a && 11 != a)){
						a++;
						advance_skip_whitespace(&ch);
					}else if('-' == *ch && (0 != a && 5 != a)){
						a--;
						advance_skip_whitespace(&ch);
					}
				}
				a += 12*octave;

				b = 0; // count of number of dots
				while(NULL != *ch && '.' == *ch){
					b++;
					advance_skip_whitespace(&ch);
				}
				d = (b+2)*d/2; // potential overflow

				if(a >= 0 && a <= 84){
					a = note_freq[a];
					c = GetPlayDuration(d);
					beeper.Beep(a, c); Sleep(d-c);
				}
				break;
			case 'n':
			case 'N': // play note, followed by 0-84, 0 is rest
				advance_skip_whitespace(&ch);
				d = GetDuration(0);
				a = 0;
				while(NULL != *ch){ // potential overflow
					b = *ch - '0';
					if(0 <= b && b <= 9){
						a = 10*a + b;
						advance_skip_whitespace(&ch);
					}else{ break; }
				}
				
				b = 0; // count of number of dots
				while(NULL != *ch && '.' == *ch){
					b++;
					advance_skip_whitespace(&ch);
				}
				d = (b+2)*d/2; // potential overflow

				if(a <= 84){
					if(0 == a){
						Sleep(GetDuration(0));
					}else{
						c = GetPlayDuration(d);
						beeper.Beep(note_freq[a-1], c); Sleep(d-c);
					}
				}
				break;
			case 'l':
			case 'L': //length
				advance_skip_whitespace(&ch);
				a = 0;
				while(NULL != *ch){ // potential overflow
					b = *ch - '0';
					if(0 <= b && b <= 9){
						a = 10*a + b;
						advance_skip_whitespace(&ch);
					}else{ break; }
				}
				if(a < 1){ a = 1; }
				if(a > 64){ a = 64; }
				note_length = a;
				break;
			case 'm':
			case 'M': // music commands
				advance_skip_whitespace(&ch);
				if(NULL != *ch){
					switch(*ch){
					case 'L':
						style = 2;
						break;
					case 'N':
						style = 1;
						break;
					case 'S':
						style = 0;
						break;
					case 'F':
						foreground = 1;
						break;
					case 'B':
						foreground = 0;
						break;
					default:
						break;
					}
					// skip to next char, since we don't know this command
					advance_skip_whitespace(&ch);
				}
				break;
			case 'p':
			case 'P': // pause, can take a suffix
				advance_skip_whitespace(&ch);
				a = 0;
				while(NULL != *ch){ // potential overflow
					b = *ch - '0';
					if(0 <= b && b <= 9){
						a = 10*a + b;
						advance_skip_whitespace(&ch);
					}else{ break; }
				}
				if(a < 1){ a = 1; } // clamp before or after dots?
				if(a > 64){ a = 64; }
				d = a;
				
				b = 0; // count of number of dots
				while(NULL != *ch && '.' == *ch){
					b++;
					advance_skip_whitespace(&ch);
				}
				d = (b+2)*d/2; // potential overflow

				Sleep(GetDuration(d));
				break;
			case 't':
			case 'T': // tempo
				advance_skip_whitespace(&ch);
				a = 0;
				while(NULL != *ch){ // potential overflow
					b = *ch - '0';
					if(0 <= b && b <= 9){
						a = 10*a + b;
						advance_skip_whitespace(&ch);
					}else{ break; }
				}

				if(a < 32){ a = 32; }
				if(a > 255){ a = 255; }
				tempo = a;
				break;
			default:
				advance_skip_whitespace(&ch);
				break;
			}
		}
	}
};
