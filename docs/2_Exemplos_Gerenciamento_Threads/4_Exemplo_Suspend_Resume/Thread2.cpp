/* Thread2.cpp - Generated by Visual Multi-Thread Win32 */

#include "AppObjects.h"
#include  <conio.h>
#include <stdio.h>
extern volatile UINT count;

DWORD WINAPI thread_Thread2(LPVOID lpParameter);

DWORD WINAPI thread_Thread2(LPVOID lpParameter)
{
	int x=10;
	
	printf("Executando Thread2 - Thread1 vai ser suspenso\n");
	SuspendThread(Thread1);

	while(x>0)
	{
		printf("                            Executando Thread2 - \n");
		x--;														  
		Sleep(50);
	}

	printf("Executndo thread2 - Thread1 vai ser reiniciado\n");
	ResumeThread(Thread1);
	return 0;
}
