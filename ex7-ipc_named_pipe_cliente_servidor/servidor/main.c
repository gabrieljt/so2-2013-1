/* 
UFSCar, Sistemas Operacionais 2, Exercício 7 - IPC Named Pipe - Cliente Servidor
Criado por Gabriel Jordão Trabasso
RA: 298573
Primeiro Semestre 2013
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>

#define SERVER_TIMEOUT 400000
#define SERVER_BUFFER_SIZE 1024
#define BUFFER_SIZE 128

#define STATE_ACCEPT 1
#define STATE_RESPONSE 2
#define STATE_CLOSE 3

INT currentState = STATE_ACCEPT;

VOID ThreadServer(LPVOID);
DWORD main(VOID);

DWORD main(VOID) {
	BOOL connected;
	DWORD threadServerId;
	HANDLE server, threadServer;
	LPTSTR serverName = "\\\\.\\pipe\\serverPipe";

	server = CreateNamedPipe(serverName, PIPE_ACCESS_DUPLEX, PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,
			PIPE_UNLIMITED_INSTANCES, SERVER_BUFFER_SIZE, SERVER_BUFFER_SIZE, SERVER_TIMEOUT, NULL);

	printf("[INFO] Servidor %s criado. Esperando Cliente...\n\n", serverName);

	do {
		connected = ConnectNamedPipe(server, NULL);
		if (connected)	{
			threadServer = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) ThreadServer, (LPVOID) server,
				0, &threadServerId);		
			WaitForSingleObject(threadServer, INFINITE);
		} else {
			printf("[ERROR] Cliente não conseguiu conectar.\n");
			CloseHandle(server);
		}
	} while (connected);

	printf("[WARNING] Pressione qualquer tecla para encerrar.");
	getch();
	return 1;
}

VOID ThreadServer(LPVOID serverHandle) {
	CHAR receiveMessage[BUFFER_SIZE] = "";
	DWORD bytesReceived = 0;
	CHAR sendMessage[BUFFER_SIZE] = "";
	DWORD bytesSent = 0;
	BOOL success;	
	INT sleepTime;
	HANDLE server = (HANDLE) serverHandle;
	
	do {
		success = ReadFile(server, receiveMessage, BUFFER_SIZE, &bytesReceived, NULL);
		if (success) {
			switch (currentState) {
				case STATE_ACCEPT:
					printf("[INFO] Cliente %s se conectou. Enviando ACK.\n\n", receiveMessage);						
					strcpy(sendMessage, "[INFO] Resposta Servidor: ACK.");
					success = WriteFile(server, sendMessage, BUFFER_SIZE, &bytesSent, NULL);
					if (success) { 
						currentState = STATE_RESPONSE;
						FlushFileBuffers(server);										
					}
					break;
				case STATE_RESPONSE:
					printf("[INFO] Cliente requisitou %s.\n\n", receiveMessage);						
					strcpy(sendMessage, "[INFO] Resposta Servidor: operação realizada.");
					success = WriteFile(server, sendMessage, BUFFER_SIZE, &bytesSent, NULL);
					if (success) {
						currentState = STATE_CLOSE;
						FlushFileBuffers(server);										
					}
					break;
				case STATE_CLOSE:
					printf("[INFO] Cliente requisitou %s.\n\n", receiveMessage);						
					strcpy(sendMessage, "[INFO] Resposta Servidor: encerrando conexão.");
					success = WriteFile(server, sendMessage, BUFFER_SIZE, &bytesSent, NULL);
					if (success) {
						currentState = STATE_ACCEPT;						
						success = FALSE;
						FlushFileBuffers(server);										
					}
					break;
				default:
					success = FALSE;					
			}			
		} 
		strcpy(receiveMessage, "");
		bytesReceived = 0;
		strcpy(sendMessage, "");
		bytesSent = 0;
		Sleep(100);
	} while (success);
	
	DisconnectNamedPipe(server);
}
