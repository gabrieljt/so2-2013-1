/* 
UFSCar, Sistemas Operacionais 2, Exercício 7 - IPC Named Pipe - Cliente Servidor
Criado por Gabriel Jordão Trabasso
RA: 298573
Primeiro Semestre 2013
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

#define CLIENT_TIMEOUT 5000
#define BUFFER_SIZE 128

#define STATE_CONNECT 1
#define STATE_REQUEST 2
#define STATE_CLOSE 3

DWORD main(VOID);

DWORD main(VOID) {
	CHAR sendMessage[BUFFER_SIZE] = "";
	DWORD bytesSent = 0;
	CHAR receiveMessage[BUFFER_SIZE] = "";
	DWORD bytesReceived = 0;
	BOOL success;	
	INT sleepTime;
	HANDLE server;
	CHAR * serverName = "\\\\127.0.0.1\\pipe\\serverPipe";
	INT currentState = STATE_CONNECT;

	printf("[INFO] Esperando Servidor: %s\n", serverName);
	while (!WaitNamedPipe(serverName, INFINITE)) { 
		printf(".");
		Sleep(500);
	}
	printf("\n[INFO] Conectado ao Servidor.\n\n");

	strcpy(sendMessage, "<NOME_CLIENTE>");
	do {
		server = CreateFile(serverName,	GENERIC_WRITE | GENERIC_READ, 0, 
		NULL, OPEN_EXISTING, 0,	NULL);
		if (server == INVALID_HANDLE_VALUE) {
			Sleep(100);
		} else {	
			success = WriteFile(server, sendMessage, BUFFER_SIZE, &bytesSent, NULL);
			Sleep(100);
			if (success) {
				switch (currentState) {
					case STATE_CONNECT:
						success = ReadFile(server, receiveMessage, BUFFER_SIZE, &bytesReceived, NULL);
						printf("%s\n\n", receiveMessage);						
						if (success) {
							currentState = STATE_REQUEST;
							strcpy(sendMessage, "WRITE_FILE");
						}
						break;
					case STATE_REQUEST:
						success = ReadFile(server, receiveMessage, BUFFER_SIZE, &bytesReceived, NULL);
						printf("%s\n\n", receiveMessage);						
						if (success) {
							currentState = STATE_CLOSE;
							strcpy(sendMessage, "CLOSE_CONNECTION");
						}
						break;
					case STATE_CLOSE:
						success = ReadFile(server, receiveMessage, BUFFER_SIZE, &bytesReceived, NULL);
						printf("%s\n\n", receiveMessage);						
						if (success) 
							success = FALSE;
						break;
					default:
						success = FALSE;					
				}			
			}
			strcpy(receiveMessage, "");
			bytesReceived = 0;
			bytesSent = 0;
			CloseHandle(server);
			Sleep(200);
		} 
	} while (success);

	

	printf("[WARNING] Pressione qualquer tecla para encerrar.");
	getch();
	return 1;
}
