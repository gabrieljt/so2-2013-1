/* Thread1.cpp - Generated by Visual Multi-Thread Win32 */

#include "AppObjects.h"
#include <stdio.h>
#include <stdlib.h>

DWORD WINAPI thread_Thread1(LPVOID lpParameter);

DWORD WINAPI thread_Thread1(LPVOID lpParameter)
{
	int i;
	
	for(i=1;i<=20;i++)
	{
		printf("Thread1 executando,   Valor de i = %d\n", i);
		
		Sleep(500);
	}
	return 0;
}
