/* 
UFSCar, Sistemas Operacionais 2, Exercício 6 - IPC Named Pipe - Produtor Consumidor
Criado por Gabriel Jordão Trabasso
RA: 298573
Primeiro Semestre 2013
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

#define PIPE_TIMEOUT 5000
#define BUFFER_SIZE 10

void randomString(char * string, size_t length) {
	/* ASCII characters 33 to 126 */
	int numChars = length - 1;
	int i;
	for (i = 0; i < numChars; ++i) {
		string[i] = rand() % (126 - 33 + 1) + 33;
	}
	 
	string[numChars] = '\0';  
}

DWORD main(VOID);

DWORD main(VOID) {
	CHAR item[BUFFER_SIZE] = "";
	DWORD bytesProduced = 0;
	BOOL success;	
	INT sleepTime;
	HANDLE pipe;
	CHAR * pipeName = "\\\\.\\pipe\\pipeProducerConsumer";

	printf("[INFO] Esperando Named Pipe: %s\n", pipeName);
	while (!WaitNamedPipe(pipeName, INFINITE)) { 
		printf(".");
		Sleep(500);
	}
	printf("\n[INFO] Conectado ao Consumidor.\n\n");

	pipe = CreateFile(pipeName,	GENERIC_WRITE, 0, 
		NULL, OPEN_EXISTING, 0,	NULL);

	if (pipe == INVALID_HANDLE_VALUE) {
		printf("[ERROR] Erro ao criar Named Pipe.\n");
	} else {
		do {
			randomString(item, BUFFER_SIZE);
			success = WriteFile(pipe, item, BUFFER_SIZE, &bytesProduced, NULL);
			if (success) {
				printf("[INFO] Produtor escreveu item %s no pipe.\n\n", item);
				sleepTime = 1000 + (rand() % 1000);	
				Sleep(sleepTime);
			}
		} while (success);
	}

	printf("[ERROR] Produtor não conseguiu escrever no pipe.\n");	
	CloseHandle(pipe);

	printf("[WARNING] Pressione qualquer tecla para encerrar.");
	getch();
	return 1;
}
