/* 
UFSCar, Sistemas Operacionais 2, Exercício 6 - IPC Named Pipe - Produtor Consumidor
Criado por Gabriel Jordão Trabasso
RA: 298573
Primeiro Semestre 2013
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>

#define PIPE_TIMEOUT 400000
#define PIPE_BUFFER_SIZE 100
#define BUFFER_SIZE 10

VOID ThreadConsumer(LPVOID);
DWORD main(VOID);

DWORD main(VOID) {
	BOOL connected;
	DWORD threadConsumerId;
	HANDLE pipe, threadConsumer;
	LPTSTR pipeName = "\\\\.\\pipe\\pipeProducerConsumer";

	pipe = CreateNamedPipe(pipeName, PIPE_ACCESS_DUPLEX, PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,
			PIPE_UNLIMITED_INSTANCES, PIPE_BUFFER_SIZE, PIPE_BUFFER_SIZE, PIPE_TIMEOUT, NULL);

	printf("[INFO] Named Pipe %s criado.\nEsperando Produtor...\n\n", pipeName);

	connected = ConnectNamedPipe(pipe, NULL);
	if (connected)	{
		threadConsumer = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) ThreadConsumer, (LPVOID) pipe,
			0, &threadConsumerId);		
		WaitForSingleObject(threadConsumer, INFINITE);
	} else {
		printf("[ERROR] Produtor não conseguiu conectar.\n");
		CloseHandle(pipe);
	}

	printf("[WARNING] Pressione qualquer tecla para encerrar.");
	getch();
	return 1;
}

VOID ThreadConsumer(LPVOID pipeHandle) {
	CHAR item[BUFFER_SIZE] = "";
	DWORD bytesConsumed = 0;
	BOOL success;	
	INT sleepTime;
	HANDLE pipe = (HANDLE) pipeHandle;
	
	do {
		success = ReadFile(pipe, item, BUFFER_SIZE, &bytesConsumed, NULL);
		if (success) {
			printf("[INFO] Consumidor leu item %s do pipe.\n\n", item);						
			FlushFileBuffers(pipe);
			sleepTime = 2000 + (rand() % 2000);
			Sleep(sleepTime);	
		} 
	} while (success);
	
	printf("[ERROR] Consumidor não conseguiu ler do pipe.\n");	
	DisconnectNamedPipe(pipe);
	CloseHandle(pipe);	
}
