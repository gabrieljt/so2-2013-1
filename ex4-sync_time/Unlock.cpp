/* Unlock.cpp - Generated by Visual Multi-Thread Win32 */

#include "AppObjects.h"

extern volatile LONG lock;

void Unlock(LONG lock);

void Unlock(LONG lock)
{
	LeaveCriticalSection(&Section);
	InterlockedExchange(&lock, 0);
}
